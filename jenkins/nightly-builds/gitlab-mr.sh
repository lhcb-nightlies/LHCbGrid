#!/bin/bash -e
###############################################################################
# (c) Copyright 2013-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

. $(dirname $0)/../utils.sh

set_common

rm -f gitlab-*-build.txt

lbn-gitlab-mr \
    --debug \
    --hook-var "content" \
    --output "gitlab-slots-build.txt" \
    --feedback
