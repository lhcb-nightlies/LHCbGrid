###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This script prepares the environment to run the nightly builds tasks

# Validate the execution environment
if [ -z "$WORKSPACE" ] ; then
  echo 'error: the environment does not look like a Jenkins job ($WORKSPACE not defined)'
  exit 1
fi

if [ ! -r "$WORKSPACE/$(basename ${BASH_SOURCE[0]})" ] ; then
  echo 'error: $WORKSPACE should point to the directory containing this file'
  exit 1
fi

if [ -n "$TMPDIR" -a ! -d "$TMPDIR" ] ; then
  mkdir -p "$TMPDIR"
fi

os=${platform#*-}
os=${os%%-*}
venv=$WORKSPACE/venv/$os
: ${XDG_CACHE_HOME:=$venv/.cache}
export XDG_CACHE_HOME

# Get usable Python version
case $os in
  slc5 )
    python=/cvmfs/lhcb.cern.ch/lib/lcg/external/Python/2.7.3/x86_64-slc5-gcc46-opt/bin/python
    ;;
  slc6 )
    python=/cvmfs/lhcb.cern.ch/lib/lcg/releases/Python/2.7.15-c333c/x86_64-slc6-gcc8-opt/bin/python
    ;;
  * )
    # on any OS other than SLC5/6, assume system python is good enough
    python=python
    ;;
esac

set +e
# a bug in pip 20.0 makes virtual envs unusable
if [ -e "$venv/lib/python2.7/site-packages/pip-20.0.dist-info" ] ; then
  rm -f $venv/bin/activate
fi
# the upgrade from smmap2==2.0.5 to smmap2==3.0.1 seems to corrupt the installation
if [ -e "$venv/lib/python2.7/site-packages/smmap2-2.0.5.dist-info" ] ; then
  rm -f $venv/bin/activate
fi
if [ ! -e "$venv/lib/python2.7/site-packages/smmap" ] ; then
  rm -f $venv/bin/activate
fi
if [ -r "$venv/bin/activate" ] ; then
  echo "check usability of $venv"
  # check if the virtualenv make sense (we may have path mismatch)
  tgt=$(. $venv/bin/activate 2>/dev/null && echo $VIRTUAL_ENV)
  if [ "${tgt%%/}" != "${venv%%/}" ] ; then
    echo "cannot use $venv (it thinks it is $tgt)"
    rm -f $venv/bin/activate
  fi
  unset tgt
fi

set -e
if [ ! -r "$venv/bin/activate" -o "$JENKINS_RESET_VIRTUALENV" = "true" ] ; then
  echo "installing fresh virtualenv in $venv"
  rm -rf "$venv"
  # bootstrap a new virualenv using virtualenv from sources
  pip_index=https://lhcb-repository.web.cern.ch/repository/pypi/simple
  tmp=$(mktemp -d)
  virtualenv_version=16.7.5
  if [ ! -r $XDG_CACHE_HOME/virtualenv-${virtualenv_version}.tar.gz ] ; then
    mkdir -p $XDG_CACHE_HOME
    virtualenv_url=${pip_index}/virtualenv/$(curl ${pip_index}/virtualenv/ 2>/dev/null | awk -F'"' '/virtualenv-'"${virtualenv_version}"'.tar.gz/{print $2}')
    curl --location --output $XDG_CACHE_HOME/virtualenv-${virtualenv_version}.tar.gz ${virtualenv_url%#*}
  fi
  tar -x -f $XDG_CACHE_HOME/virtualenv-${virtualenv_version}.tar.gz --strip-components=1 -C $tmp
  if [ "$os" = slc6 ] ; then
    # On SLC6 we have to copy libpython to the virtualenv we are going to create
    # (code borrowed from https://gitlab.cern.ch/lhcb-core/lbenv-deployment/blob/master/jenkins/prepare_kit)
    mkdir -p $venv/lib
    libpython=$(ldd $python | awk '/libpython/{print $3}')
    if [ -n "$libpython" ] ; then
      cp $libpython $venv/lib
    fi
    unset libpython
  fi
  mkdir -p $venv
  cat > $venv/pip.conf <<EOF
[global]
index-url = ${pip_index}
EOF
  $python $tmp/virtualenv.py --prompt '(nightlies) ' --no-setuptools $venv
  rm -rf $tmp
  unset tmp
fi

. $venv/bin/activate
pip install --upgrade 'setuptools<45.0'
pip install --upgrade pip
# workaround for installation on SLC5
pip install --upgrade setuptools_scm nose

(
  if [ "$os" = slc6 ] ; then
    # FIXME: using SFT build of Python for SLC6 requires binutils in the PATH,
    # in case we have to build a binary module (e.g. backports.lzma)
    export PATH=/cvmfs/sft.cern.ch/lcg/releases/binutils/2.30/x86_64-slc6/bin:$PATH
  fi
  
  pip install -r $WORKSPACE/requirements.txt

  if [ -n "$JENKINS_OVERRIDE_PIP_REQUIREMENTS" ] ; then
    pip install $JENKINS_OVERRIDE_PIP_REQUIREMENTS
  fi
)
set +e
