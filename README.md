# Nightly Builds Jenkins Scripts

Scripts used in the Jenkins Nightly Builds jobs.


## Introduction

As described in [LBCORE-1772](https://its.cern.ch/jira/browse/LBCORE-1772), we
decided to separate the Jenkins logic from the actual Nightly Builds logic.

This project collects the entry points used in Jenkins to prepare
([prepare_env.sh](prepare_env.sh)) the local installation of the Python tools
constituting the main logic of the [LHCb Nightly Builds][], and then invoke the
actions specific to each Jenkins job part of the Nightly Builds Workflow.


## The Python tools

The list of Python packages required to run the [LHCb Nightly Builds][] is
recorded in the [pip][] requirements file [requirements.txt.in](requirements.txt.in),
which, to ensure stability and reproducibility of the jobs, is processed via
[pip-compile][] to produce the actually used file [requirements.txt](requirements.txt).

When needed, the [requirements.txt](requirements.txt) can be updated with
```
pip-compile --index-url=https://lhcb-repository.web.cern.ch/repository/pypi/simple --output-file=requirements.txt requirements.txt.in
```

For testing it's possible to temporary modify the content of the toolbox via
the environment variable `JENKINS_OVERRIDE_PIP_REQUIREMENTS`, to be set to a
valid set of options to pass to the command `pip install`.



[LHCb Nightly Builds]: ...
[pip]: https://pip.pypa.io/en/stable/
[pip-compile]: https://github.com/jazzband/pip-tools/
